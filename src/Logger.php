<?php
/**
 * @author Zac Fowler <zfowler@unomaha.edu>
 */

namespace Attic\Logging;

use Psr\Log\AbstractLogger;
use Psr\Log\LogLevel;

class Logger extends AbstractLogger
{
    private $defaultLogPath;

    public function __construct($defaultLogPath = '')
    {
        if (!empty($defaultLogPath)) {
            $this->defaultLogPath = $defaultLogPath;
        } else {
            throw new \Exception("Log Path not defined.");
        }
    }

    /**
     * Logs a simple message, using one of the defined subsystems
     *
     * @param string $message Message to log with optional placeholders
     * @param string $subsystem Default general
     * @param string $username Username to log
     * @internal param array $context
     */
    static function logSystem($message, $subsystem = 'general', $username = 'nobody')
    {
        $context['subsystem'] = $subsystem;
        $context['message'] = $message;
        $context['username'] = $username;
        $msgFormat = "{subsystem} {username} {message}";
        $l = new Logger();
        $msg = $l->interpolate($msgFormat, $context);
        $l->log(LogLevel::INFO, $msg, $context);
    }

    /**
     * Interpolates context values into the message placeholders.
     * @param $message
     * @param array $context
     * @return string
     */
    private function interpolate($message, array $context = array())
    {
        // build a replacement array with braces around the context keys
        $replace = array();
        foreach ($context as $key => $val) {
            $replace['{' . $key . '}'] = $val;
        }

        // interpolate replacement values into the message and return
        return strtr($message, $replace);
    }

    /**
     * Logs a message
     *
     * Logs a message by date, IP address if available, and message.
     *
     * @param mixed $level Use Psr\Log\LogLevel constants
     * @param string $message
     * @param array $context
     * @return null
     */
    public function log($level, $message, array $context = array())
    {
        $context['datetime'] = date(DATE_W3C);
        $context['message'] = str_replace("\n", '', $message);
        $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0';
        $context['ip'] = $ip;
        $msg = "{datetime} {ip} {message}";
        $log_line = $this->interpolate($msg, $context) . PHP_EOL;
        @file_put_contents($this->defaultLogPath, $log_line, FILE_APPEND);
    }


}
